"""
SOM Classification (T.Kohonen, 2001) for geotiff and subsets similarities (Jaccard index and Euclidean distances) with graph exportation.
"""
import os
import numpy as np
from src import loadgeotif
from src import somPak
import matplotlib.pyplot as plt
import gdal
from osgeo import osr

# part 1
# Load and stack data from stocked in geotif format

# geotiff location
rootdir = 'dat'

# Set extent 
inband = 1
xmin = 530000.0
xmax = 550000.0
ymin = 150000.0
ymax = 170000.0
res = 100.0

# creat empty object 'data' to stack later geotiff
data = np.zeros((int((ymax-ymin)/res), int((xmax-xmin)/res), len(os.listdir(rootdir))))

# load all geotif and stack them in the previous objet 'data'
i = 0
for subdir, dirs, files in os.walk(rootdir):
	for file in files:
		if (i == 0):
			data[:,:,i] = loadgeotif(file, subdir, inband, xmin, xmax, ymin, ymax, res)
			i += 1
		else:
			data[:,:,i] = loadgeotif(file, subdir, inband, xmin, xmax, ymin, ymax, res)
			i += 1

## preview of the extent loaded 
preview_dim = 0 # set which layer/dimension
plt.imshow(data[:, :, preview_dim], origin='lower')
plt.show(block=False)
xdata = np.shape(data[:, :, preview_dim])[0]
ydata = np.shape(data[:, :, preview_dim])[1]

## if necessary, uncomment the below code to save as geotif a specific layer from data. 
# driver = gdal.GetDriverByName('GTiff')
# dst_ds = driver.Create('out/origin_data.tif', np.shape(data)[1], np.shape(data)[0], 1, gdal.GDT_Byte)
# dst_ds.SetGeoTransform([xmin, res, 0, ymin, 0, res])
# srs = osr.SpatialReference()
# srs.ImportFromEPSG(21781)
# dst_ds.SetProjection(srs.ExportToWkt())
# dst_ds.GetRasterBand(1).WriteArray(data[:, :, preview_dim])

# part 2
# Run SOM classification

# set SOM parameters
xgrid = 5
ygrid = 5
ngrid = xgrid*ygrid
topo = 'rect'
neigh = 'gaussian'
ite = 20000
a = 10
r = 1

# SOM
mapp_list, qr = somPak(data, xgrid, ygrid, topo, neigh, ite, a, r)
mapp_npmat = np.matrix(mapp_list)

# get the BMU classification label number is the product of the neuronal grid of coordinates = position
l = []
for i in range(np.shape(mapp_npmat)[0]-1):
	l_num = mapp_npmat[i,0]*mapp_npmat[i,1]
	l.append(l_num) 

l_mat = np.matrix(l)[0,:].reshape((np.shape(data)[0],np.shape(data)[1]))

cmap = plt.cm.get_cmap('Set1', ngrid)

fig1 = plt.figure(1)
plt.imshow(l_mat, origin='lower', cmap=cmap, interpolation='nearest')
text = 'Som: xgrid: '+str(xgrid)+', ygrid: '+str(ygrid)+', topo: '+str(topo)+', neigh: '+str(neigh)+', ite: '+str(ite)+',\n a: '+str(a)+', r: '+str(r)+', qr: '+str(qr)+'\n\n'
plt.annotate(text, xy=(0., np.float(ydata-10)), xytext=(0.,np.float(ydata-10)))
cax1 = plt.axes([0.85, 0.1, 0.075, 0.8])
cb1 = plt.colorbar(ticks=range(ngrid), cax=cax1)
plt.clim(-0.5, ngrid -0.5)
labels = np.arange(1,ngrid+1,1)
cb1.set_ticklabels(labels)
plt.show(block=False)
plt.savefig('out/som_xgrid'+str(xgrid)+'_ygrid'+str(ygrid)+'_topo'+str(topo)+'_neigh'+str(neigh)+'_ite'+str(ite)+'_a'+str(a)+'_r'+str(r)+'_qr'+str(qr)+'.png', bbox_inches='tight', dpi=300)

## if necessary, uncomment the below code to save as geotif RGB.
# r = []
# g = []
# b = []
# for i in range(np.shape(mapp_npmat)[0]-1):
# 	b_red = (mapp_npmat[i,0] - mapp_npmat[i,0])+20.0
# 	r.append(b_red)
# 	b_green = (np.float(mapp_npmat[i,1])/ np.float(ygrid))*250.0
# 	g.append(b_green)
# 	b_blue = (np.float(mapp_npmat[i,0])/ np.float(xgrid))*250.0
# 	b.append(b_blue)

# driver = gdal.GetDriverByName('GTiff')
# dst_ds = driver.Create( 'out/cl_data.tif', ((int(xmax) - int(xmin))/int(res)), ((int(ymax) - int(ymin))/int(res)), 3, gdal.GDT_Byte)
# dst_ds.SetGeoTransform( [int(xmin), int(res), 0, int(ymin), 0, int(res)] )
# srs = osr.SpatialReference()
# srs.ImportFromEPSG(21781)
# dst_ds.SetProjection( srs.ExportToWkt() )
# dst_ds.GetRasterBand(1).WriteArray( np.matrix(r)[0,:].reshape((np.shape(data)[0],np.shape(data)[1] )))
# dst_ds.GetRasterBand(2).WriteArray( np.matrix(g)[0,:].reshape((np.shape(data)[0],np.shape(data)[1] )))
# dst_ds.GetRasterBand(3).WriteArray( np.matrix(b)[0,:].reshape((np.shape(data)[0],np.shape(data)[1] )))


##################################################### part 2
## Dissimilarity between subsets

# Set split
subsetx = 4
subsety = 4
nsubset = subsetx*subsety
l_h = np.hsplit(l_mat, subsetx)

list_subset = []
mat_subset = []
dlist=[]
x_sub = []
y_sub = []
nsubh = np.shape(l_h)[0]
nsubv = np.shape(l_h)[0]
for i in range(nsubh):
	l_v = np.vsplit(l_h[i], subsety)
	for j in range(nsubv):
		list_subset.append(np.array(l_v[j]).reshape(-1,).tolist())
		mat_subset.append(l_v[j])
		unique, counts = np.unique(np.array(l_v[j]).reshape(-1,).tolist(), return_counts=True)
		dlist.append(np.asarray(unique))
		y_sub.append(j)
		x_sub.append(i)

scale = np.float((np.float(xmax)-np.float(xmin))/subsetx), np.float((np.float(ymax)-np.float(ymin))/subsety)
content = open('out/network_n'+str(subsetx)+'.csv', 'w')
nlist = np.shape(dlist)[0]
content.write('x1, y1, x2, y2, Jaccard dissimilarity, Euclidean distance (m)\n')
for k in range(nlist):
	for p in range(nlist):
		r = []
		jaccard_index = np.float(1)-(np.float(len(np.intersect1d(dlist[k], dlist[p])))/ len(np.union1d(dlist[k], dlist[p])))
		euclidean_distance = np.sqrt((np.float((x_sub[k]+1)*scale[0]) - np.float((x_sub[p]+1)*scale[0]))**2 + (np.float((y_sub[k]+1)*scale[1]) -  np.float((y_sub[p]+1)*scale[1]))**2)
		r = [x_sub[k]+1, y_sub[k]+1, x_sub[p]+1, y_sub[p]+1, jaccard_index, euclidean_distance]
		content.write(str(r).replace('[','').replace(']','')+'\n')

fig2 = plt.figure(2)
for i in xrange(0, nsubset):
	plt.subplot2grid((subsety,subsetx), ((subsety-1)-y_sub[i], x_sub[i]))
	ax = plt.imshow(mat_subset[i], origin='lower', cmap=cmap, vmin=0, vmax=ngrid, interpolation='nearest')
	ax.axes.set_xticklabels([])
	ax.axes.set_yticklabels([])
	
plt.subplots_adjust(wspace=-0.5, hspace=0.08)
cax2 = plt.axes([0.85, 0.1, 0.075, 0.8])
cb2 = plt.colorbar(ticks=range(ngrid), cax=cax2)
plt.clim(-0.5, ngrid -0.5)
cb2.set_ticklabels(labels)
fig2.suptitle('Subsets - hsplit: '+str(subsetx)+', vsplit: '+str(subsety))
plt.savefig('out/subset_h'+str(subsetx)+'_v'+str(subsetx)+'.png', bbox_inches='tight', dpi=300)
plt.show()