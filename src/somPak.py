"""
Set SOM package and extract model from initial vectors and return BMU coordinates of learned intial vectors and qerror
Copyright 2015 - Raphael Cere
Universite de Lausanne 
"""
def somPak(data, xgrid, ygrid, topo, neigh, ite, a, r):
	import os
	import numpy as np
	import sompak
	from sompak import *
	import time

	start = time.time()
	time.time()
	print 'Apprentissage SOM...'

	base_path = os.getcwd()
	indicescl = base_path+'/out/indicescl.dat'
	codevectors = base_path+'/out/som-code-vectors.csv'
	cvqerror = base_path+'/out/som-qerror.csv'
	mappi = base_path+'/out/som-mappings.csv'

	print 'Load initial vectors...'
	vd = np.empty((1, np.shape(data)[2]))
	n = np.shape(data)[0]*np.shape(data)[1]
	for i in range(np.shape(data)[0]):
		for j in range(np.shape(data)[1]) :
			v = data[i,j,:]
			vd = np.vstack([vd, v])
   
	print 'Set and train SOM...'
	som = sompak.SOM(data=vd[:,:], shape=(xgrid,ygrid), topology=topo, neighbourhood=neigh)
	som.train(rlen=ite, alpha=a, radius=r)

	print 'Extraction of model...'
	cvec = som.code_vectors()
	# f = open(codevectors, 'w')
	# vl = len(cvec[1,1,:])
	# sep = ','
	# for j in range(int(xgrid)):
	#     for i in range(int(ygrid)):
	#         vn = []
	#         for vx in range(vl):
	#             v = cvec[i,j,vx]
	#             vn.append(v)
	#         f.write('%i,%i,%s\n' % (i, j, str(vn).replace('[','').replace(']','')))
	# f.close()

	print 'Extract qerror...'
	qer = som.qerror()
	print '-------------------'
	print 'qerror: '+str(qer)
	print '-------------------'
	# f = open(cvqerror,'w')
	# f.write(str(qer))
	# f.close()

	print 'Extact initial vectors with BMU positions...'
	mapp = som.mappings()
	# f = open(mappi,'w')
	# for m in mapp:
	#     f.write(str(m).replace('[','').replace(']','')+'\n')
	# indsc = open(indicescl, 'w')
	# ct = 0
	# for i in range(np.shape(data)[0]):
	#     indcl = []
	#     v = data[i,:]
	#     indcl.append([v, mapp[ct]])
	#     indsc.write(str(indcl).replace('[','').replace(']','')+'\n')
	#     ct = ct+1
	# f.close()

	end = time.time()
	time = end-start
	print "Done in %f seconds" % time
	return mapp, qer