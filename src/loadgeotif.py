"""
Extract features from specific dand of geotiff and retrun numpy matrix
Copyright 2017 - Raphael Cere
Universite de Lausanne 
"""
def loadgeotif(f, subdir, inband, xmin, xmax, ymin, ymax, res):
    """
    f: name of the file
    subdir: path of the file
    inband: which band of the geotiff
    xmin: minimum of the longitude projected coordinates
    xmax: maximum of the longitude projected coordinates
    ymin: minimum of the latitude projected coordinates
    ymax: maximum of the latitude projected coordinates
    res: resolution of the pixel
    """
    import os
    import numpy as np
    import gdal
    from osgeo import osr
    import time

    start = time.time()
    time.time()   
    print 'Read raster: '+str(f)

    rootdir = os.getcwd()
    inputfile = rootdir+'/'+subdir+'/'+f
    
    in_ds = gdal.Open(inputfile, gdal.GA_ReadOnly) # Read f
    geotransform = in_ds.GetGeoTransform()         # get coordinates and resolution
    in_band = in_ds.GetRasterBand(inband)          # Read band
    datatype = in_band.DataType                    # Type of data
    in_size = in_band.XSize, in_band.YSize         # Size of file
    oxoy = int((xmin - int(geotransform[0]))/100.0), int((ymin - int(geotransform[3]))/100.0)
    size = (int(xmax)-int(xmin))/int(res), (int(ymax)-int(ymin))/int(res)
    in_data = in_band.ReadAsArray(oxoy[0], oxoy[1], size[0], size[1]).astype(np.float) # Read data in band

    ## Uncomment to save geotiff
    # driver = gdal.GetDriverByName('GTiff')
    # dst_ds = driver.Create('/out/origin_datao.tif', np.shape(in_data)[1], np.shape(in_data)[0], 1, gdal.GDT_Byte)
    # dst_ds.SetGeoTransform([xmin, res, 0.0, ymin, 0.0, res])
    # srs = osr.SpatialReference()
    # srs.ImportFromEPSG(21781)
    # dst_ds.SetProjection(srs.ExportToWkt())
    # dst_ds.GetRasterBand(1).WriteArray(in_data[:, :])

    end = time.time()
    time = end-start
    print "Done in %f seconds" % time

    return in_data